import {Server} from 'http';
import express from 'express';
import socketio from 'socket.io';

const app = express();
const http = Server(app);
const io = socketio(http);

app.use(express.static(__dirname + '/../public'));

function callback(fn) {
    fn && fn();
}

const todoLists = [];
io.on('connection', function (socket) {
    socket.emit('list', todoLists);

    socket.on('getList', function (data, cb) {
        socket.emit('list', todoLists);

        callback(cb);
    });

    socket.on('addList', function (list, cb) {
        io.emit('addList', list);

        todoLists.push(list);
        callback(cb);
    });

    socket.on('updateList', function ({listId, title}, cb) {
        io.emit('updateList', arguments[0]);

        let list = todoLists.find(el => el.id == listId);
        list.title = title;
        callback(cb);
    });

    socket.on('removeList', function (listId, cb) {
        io.emit('removeList', listId);

        let listIndex = todoLists.findIndex(el => el.id == listId);
        todoLists.splice(listIndex, 1);
        callback(cb);
    });

    socket.on('addTodo', function ({listId, todo}, cb) {
        io.emit('addTodo', arguments[0]);

        let list = todoLists.find(el => el.id == listId);
        list.todos.push(todo);
        callback(cb);
    });

    socket.on('updateTodo', function ({listId, todoId, title, completed}, cb) {
        io.emit('updateTodo', arguments[0]);

        let list = todoLists.find(el => el.id == listId);
        let todo = list.todos.find(el => el.id == todoId);
        todo.title = title;
        todo.completed = completed;
        callback(cb);
    });

    socket.on('removeTodo', function ({listId, todoId}, cb) {
        io.emit('removeTodo', arguments[0]);

        let list = todoLists.find(el => el.id == listId);
        let todoIndex = list.todos.findIndex(el => el.id == todoId);
        list.todos.splice(todoIndex, 1);
        callback(cb);
    });
});

http.listen(3000, function () {
    console.log('listening on localhost:3000');
});