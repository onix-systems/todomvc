angular.module('todomvc')
	.factory('socket', function (socketFactory) {
		var socket = socketFactory();
		socket.forward(['list', 'getList', 'addList', 'updateList', 'removeList', 'addTodo', 'updateTodo', 'removeTodo']);
		return socket;
	});