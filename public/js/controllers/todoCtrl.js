/*global angular */

/**
 * The main controller for the app. The controller:
 * - retrieves and persists the model via the todoStorage service
 * - exposes the model to the template and provides event handlers
 */
angular.module('todomvc')
	.controller('TodoCtrl', function TodoCtrl($scope, $route, $routeParams, $location, $filter, ngDialog, socket) {
		'use strict';

		$scope.todoLists = [];
		$scope.newTodo = null;
		$scope.newList = null;
		$scope.editedTodo = null;

		$scope.$on('$routeChangeSuccess', function () {

			var status = $scope.status = $routeParams.status || '';

			if (status === 'active') {
				$scope.statusFilter = {completed: false};
			}
			else if (status === 'completed') {
				$scope.statusFilter = {completed: true};
			}
			else {
				$scope.statusFilter = {};
			}
		});

		$scope.setCurrent = function(list){
			$scope.currentList = list;
		};

		socket.emit('getList');

		$scope.$on('socket:list', function (ev, data) {
			$scope.todoLists = data;
			$scope.currentList = _.find($scope.todoLists, { id: $routeParams.listId });
		});

		$scope.isActiveList = function (viewLocation) {
			return viewLocation == $route.current.params.listId;
		};

		$scope.$on('socket:addList', function (ev, data) {
			$scope.todoLists.push(data);
		});

		$scope.addList = function () {
			ngDialog.openConfirm({
				template: 'add-list.html',
				scope: $scope
			}).then(
				function (value) {
					if (value) {
						var newList = {
							id: Math.random().toString(36).substr(2, 9),
							title: value.trim(),
							todos: []
						};
						socket.emit('addList', newList);
					}
				}
			);
		};

		$scope.$on('socket:updateList', function (ev, data) {
			var index = _.indexOf($scope.todoLists, _.find($scope.todoLists, { id: data.listId }));
			$scope.todoLists[index].title = data.title;
		});

		$scope.editList = function (list) {
			$scope.newList = list.title;
			ngDialog.openConfirm({
				template: 'add-list.html',
				scope: $scope
			}).then(
				function (value) {
					if (value) {
						var newList = {
							listId: list.id,
							title: value.trim()
						};
						socket.emit('updateList', newList);
						$scope.newList = null;
					}
				}
			)
		};

		$scope.$on('socket:removeList', function (ev, data) {
			var index = _.indexOf($scope.todoLists, _.find($scope.todoLists, { id: data }));
			$scope.todoLists.splice(index, 1);
			if($scope.currentList && data == $scope.currentList.id){
				$location.path('/');
			}
		});

		$scope.removeList = function(list){
			socket.emit('removeList', list.id);
		};

		$scope.$on('socket:addTodo', function (ev, data) {
			var index = _.indexOf($scope.todoLists, _.find($scope.todoLists, { id: data.listId }));
			$scope.todoLists[index].todos.push(data.todo);

		});

		$scope.addTodo = function(){

			if($scope.newTodo && $scope.newTodo.trim()!== ''){
				var newTodo = {
					id: Math.random().toString(36).substr(2, 9),
					title    : $scope.newTodo.trim(),
					completed: false
				};

				socket.emit('addTodo', {listId: $scope.currentList.id, todo:newTodo});
				$scope.newTodo = null;
			}
		};

		$scope.$on('socket:removeTodo', function (ev, data) {
			var index = _.indexOf($scope.todoLists, _.find($scope.todoLists, { id: data.listId }));
			var indexTodo = _.indexOf($scope.todoLists[index].todos, _.find($scope.todoLists[index].todos, { id: data.todoId }));
			$scope.todoLists[index].todos.splice(indexTodo, 1);
		});

		$scope.removeTodo = function(todo){
			socket.emit('removeTodo', {listId: $scope.currentList.id, todoId: todo.id});
		};

		$scope.$on('socket:updateTodo', function (ev, data) {
			var index = _.indexOf($scope.todoLists, _.find($scope.todoLists, { id: data.listId }));
			var indexTodo = _.indexOf($scope.todoLists[index].todos, _.find($scope.todoLists[index].todos, { id: data.todoId }));
			$scope.todoLists[index].todos[indexTodo].title = data.title;
			$scope.todoLists[index].todos[indexTodo].completed = data.completed;
		});

		$scope.saveTodo = function (todo) {
			if(todo.title){
				socket.emit('updateTodo', {listId: $scope.currentList.id, todoId: todo.id, title: todo.title, completed: todo.completed});
			}
		};

		$scope.editTodo = function (todo) {
			$scope.editedTodo = todo;
			$scope.originalTodo = angular.extend({}, todo);
		};

		$scope.saveEdits = function (todo, event) {
			// Blur events are automatically triggered after the form submit event.
			// This does some unfortunate logic handling to prevent saving twice.
			if (event === 'blur' && $scope.saveEvent === 'submit') {
				$scope.saveEvent = null;
				return;
			}

			$scope.saveEvent = event;

			if ($scope.reverted) {
				// Todo edits were reverted-- don't save.
				$scope.reverted = null;
				return;
			}

			todo.title = todo.title.trim();

			if (todo.title === $scope.originalTodo.title) {
				$scope.editedTodo = null;
				return;
			}

			if(todo.title){
				socket.emit('updateTodo', {listId: $scope.currentList.id, todoId: todo.id, title: todo.title, completed: todo.completed});
				$scope.editedTodo = null;
			}
		};

		$scope.revertEdits = function (todo) {
			var indexTodo = _.indexOf($scope.currentList.todos, _.find($scope.currentList.todos, { id: todo.id }));
			$scope.currentList.todos[indexTodo] = $scope.originalTodo;
			$scope.editedTodo = null;
			$scope.originalTodo = null;
			$scope.reverted = true;
		};

		$scope.toggleCompleted = function (todo, completed) {
			if (angular.isDefined(completed)) {
				todo.completed = completed;
			}
			socket.emit('updateTodo', {listId: $scope.currentList.id, todoId: todo.id, title: todo.title, completed: todo.completed});
		};

		$scope.markAll = function (completed) {
			$scope.currentList.todos.forEach(function (todo) {
				if (todo.completed !== completed) {
					$scope.toggleCompleted(todo, completed);
				}
			});
		};

	});
