/*global angular */

/**
 * The main TodoMVC app module
 *
 * @type {angular.Module}
 */
angular.module('todomvc', ['ngRoute', 'ngDialog', 'btford.socket-io'])
    .config(function ($routeProvider) {
        'use strict';

        var routeConfig = {
            controller : 'TodoCtrl',
            templateUrl: 'templates/todomvc-index.html'
        };

        $routeProvider
            .when('/', routeConfig)
            .when('/:listId', routeConfig)
            .when('/:listId/:status', routeConfig)
            .otherwise({
                redirectTo: '/'
            });
    });
