module.exports = function (config) {
    'use strict';

    config.set({
        basePath  : '../../',
        frameworks: ['jasmine'],
        reporters : ['kjhtml'],
        files     : [
            'public/bower_components/angular/angular.js',
            'public/bower_components/lodash/lodash.js',
            'public/bower_components/angular-route/angular-route.js',
            'public/bower_components/ng-dialog/js/ngDialog.js',
            'public/bower_components/angular-mocks/angular-mocks.js',
            'public/lib/angular-socket.io-mock/angular-socket.io-mock.js',
            'public/js/**/*.js',
            'test/unit/**/*.js'
        ],
        autoWatch : true,
        singleRun : false,
        browsers  : ['Chrome', 'Firefox']
    });
};