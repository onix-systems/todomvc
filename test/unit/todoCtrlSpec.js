/*global describe, it, beforeEach, inject, expect*/
(function () {
	'use strict';

	describe('Todo Controller', function () {
		var ctrl, scope, socket, $httpBackend;

		// Load the module containing the app, only 'ng' is loaded by default.
		beforeEach(module('todomvc'));

		beforeEach(inject(function ($injector, $controller, $rootScope, _socket_) {

			$httpBackend = $injector.get('$httpBackend');
			$httpBackend.whenGET('templates/todomvc-index.html').respond(200, '');

			scope = $rootScope.$new();

			socket = _socket_;

			ctrl = $controller('TodoCtrl', {
				$scope: scope
			});
			scope.$digest();
		}));

		it('should not have an edited Todo on start', function () {
			expect(scope.editedTodo).toBeNull();
		});

		it('should not have any Lists on start', function () {
			expect(scope.todoLists.length).toEqual(0);
		});

		describe('the filter', function () {
			it('should default to ""', function () {
				scope.$emit('$routeChangeSuccess');

				expect(scope.status).toBe('');
				expect(scope.statusFilter).toEqual({});
			});

			describe('being at /active', function () {
				it('should filter non-completed', inject(function ($controller) {
					ctrl = $controller('TodoCtrl', {
						$scope: scope,
						$routeParams: {
							status: 'active'
						}
					});

					scope.$emit('$routeChangeSuccess');
					expect(scope.statusFilter.completed).toBeFalsy();
				}));
			});

			describe('being at /completed', function () {
				it('should filter completed', inject(function ($controller) {
					ctrl = $controller('TodoCtrl', {
						$scope: scope,
						$routeParams: {
							status: 'completed'
						}
					});

					scope.$emit('$routeChangeSuccess');
					expect(scope.statusFilter.completed).toBeTruthy();
				}));
			});
		});

		describe('having some saved Lists', function () {

			it('should add List', function () {
				expect(scope.todoLists.length).toEqual(0);
				socket.receive('addList', {id: 123456, title: 'list1', todos: []});
				expect(scope.todoLists.length).toEqual(1);
			});

			it('should update List', function () {
				scope.todoLists = [{id: 123456, title: 'list1', todos: []}];
				expect(scope.todoLists[0].title).toEqual('list1');
				socket.receive('updateList', {listId: 123456, title: 'list2'});
				expect(scope.todoLists[0].title).toEqual('list2');
			});

			it('should delete List', function () {
				scope.todoLists = [{id: 123456, title: 'list1', todos: []}];
				expect(scope.todoLists.length).toEqual(1);
				socket.receive('removeList', {listId: 123456});
				expect(scope.todoLists.length).toEqual(0);
			});

		});

		describe('having some saved Todos', function () {

			it('should add Todo', function () {
				scope.currentList = {id: 123456, title: 'list1', todos: []};
				scope.todoLists.push(scope.currentList);
				expect(scope.currentList.todos.length).toEqual(0);
				socket.receive('addTodo', {listId: scope.currentList.id, todo: {id: 1234, title:'todo1',completed:false}});
				expect(scope.currentList.todos.length).toEqual(1);
			});

			it('should update Todo', function () {
				scope.currentList = {id: 123456, title: 'list1', todos: [{id:1234, title:'todo1',completed:false}]};
				scope.todoLists[0] = scope.currentList;
				expect(scope.todoLists[0].todos[0].title).toEqual('todo1');
				socket.receive('updateTodo', {listId: 123456, todoId: 1234, title: 'todo2', completed:true});
				expect(scope.todoLists[0].todos[0].title).toEqual('todo2');
				expect(scope.todoLists[0].todos[0].completed).toBe(true);
			});

			it('should delete Todo', function () {
				scope.currentList = {id: 123456, title: 'list1', todos: [{id:1234, title:'todo1',completed:false}]};
				scope.todoLists[0] = scope.currentList;
				expect(scope.currentList.todos.length).toEqual(1);
				socket.receive('removeTodo', {listId: 123456, todoId: 12345});
				expect(scope.currentList.todos.length).toEqual(0);
			});

		});

	});
}());
