#!/bin/bash

function RunCommands {
    i=0
    while (( "$#" )); do
        let i=$i+1
        echo "Step $i: $1"
        $1
        shift
    done
}

function ProjectSetup {
    echo "Project setup procedure..."
    RunCommands "npm install" "bower --allow-root install" "npm install grunt" "npm install grunt-cli" "cd server" "npm install" "cd .."
    echo "Done)"
}

function ProjectRun {
    echo "Project is running..."
    RunCommands "cd server" "npm start"
}

function ProjectTest {
    echo "Run a tests procedure"
    RunCommands "npm test"
}

function Usage {
cat <<EOF
Usage: docker build -t [image name] 
       docker run --rm -v [path to the project]:/project -p 9876:9878 [image name] [parameter]

Possible parameters:
   help    - show this help
   setup   - do setup procedure on your project
   run     - run your project
   test    - test your project

EOF
}
function Error {
    if [ ! -z "$1" ]; then
	echo -e "$1"
    fi
    if [ -z "$2" ]; then Usage; fi
}

#
#
# check is prject folder empty
if [ ! -z "${PROJECT_FOLDER}" ] && [ -e "${PROJECT_FOLDER}" ]; then
#
    if [ "$(pwd)" != "${PROJECT_FOLDER}" ]; then cd ${PROJECT_FOLDER}; fi
    # check is the project folder empty
    if [ "$(ls ${PROJECT_FOLDER})" == "" ]; then Error "Error! Project folder is empty"; exit; fi
    case "$1" in 
        setup) ProjectSetup
        ;;
        test) ProjectTest
        ;;
        run) ProjectRun
        ;;
        bash) exec /bin/bash
        ;;
        help) Usage
        ;;
        *) ProjectSetup 
           ProjectTest
        ;;
    esac
else
    Error "Error! Something wrong was happened."
fi