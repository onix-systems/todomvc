FROM node:5.7.0

ENV PROJECT_FOLDER=/project \
    CHROME_BIN=google-chrome

COPY entrypoint.sh /

RUN chmod +x /entrypoint.sh

RUN mkdir -p ${PROJECT_FOLDER}

RUN npm install -g bower

WORKDIR ${PROJECT_FOLDER}

EXPOSE 3000 9876

ENTRYPOINT ["/entrypoint.sh"]