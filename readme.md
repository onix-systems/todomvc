# Pet Project - TodoMVC

### To setup project ###
* npm install
* bower install
* cd server
* npm install

### To run environment ###
* grunt
* open url http://localhost:3000/ in browser

### Using docker ###
Deploy Project with docker

* docker build -t <image name> ./
* docker run --name <container name> -v <full path to the project>:/project -p 9876:9876 <image name>
* open url http://localhost:9876/

Stop an existing container

* docker stop <container name>

Run already created container for run test

* docker start <container name>
* open url http://localhost:9876/