module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concurrent: {
            target: {
                tasks: ['watch', 'nodemon'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },

        karma: {
            unit: {
                configFile: __dirname + '/test/config/karma.conf.js'
            }
        },

        jshint: {
            gruntfile: [
                'Gruntfile.js'
            ],

            js: [
                'public/js/**/*.js'
            ],

            css: [
                'public/css/**/*.css'
            ]
        },

        watch: {
            gruntfile: {
                files: 'Gruntfile.js',
                tasks: ['jshint']
            },

            src: {
                options: {
                    livereload: true
                },

                files: [
                    'public/js/**/*.js',
                    'public/css/**/*.css',
                    'public/**/*.html',
                    '!public/lib/**'
                ],

                tasks: ['jshint', 'karma']
            }
        },

        nodemon: {
            dev: {
                script: 'index.js',
                options: {
                    nodeArgs: ['./node_modules/.bin/babel-node', '--presets', 'es2015'],
                    callback: function (nodemon) {
                        nodemon.on('log', function (event) {
                            console.log(event.colour);
                        });
                    },
                    cwd: __dirname + '/server',
                    ignore: ['node_modules/**'],
                    ext: 'js'
                }
            }
        }
    });

    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-karma');
    grunt.registerTask('default', ['concurrent:target']);

};